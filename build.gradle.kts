import com.jfrog.bintray.gradle.BintrayExtension
import nu.studer.gradle.credentials.domain.CredentialsContainer
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.util.Date

plugins {
    java
    `maven-publish`
    kotlin("jvm") version "1.3.50"
    id("com.jfrog.bintray") version "1.8.4"
    id("nu.studer.credentials") version "1.0.7"
    id("net.researchgate.release") version "2.6.0"
}

repositories {
    jcenter()
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    api(vertx("core"))
    api(vertx("lang-kotlin"))
    api(vertx("lang-kotlin-coroutines"))
    api(koin("core"))

    testCompile(junit5Jupiter("jupiter-api"))
    testCompile(junit5Jupiter("jupiter-engine"))
    testCompile(vertx("junit5"))
    testCompile(koin("test"))
    testCompile("io.kotlintest:kotlintest-runner-junit5:3.4.0")
}

fun vertx(module: String) = "io.vertx:vertx-$module:3.8.1"
fun koin(module: String) = "org.koin:koin-$module:2.0.1"
fun junit5Jupiter(module: String) = "org.junit.jupiter:junit-$module:5.5.1"


configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}
tasks.withType<Test> {
    useJUnitPlatform()
}

val credentialsContainer: CredentialsContainer = project.extra["credentials"] as CredentialsContainer
var bintrayUser = credentialsContainer.propertyMissing("bintray_user").toString()
var bintrayApiKey = credentialsContainer.propertyMissing("bintray_api_key").toString()

val publicationName = "vertxKoinVerticleFactory"

bintray {
    user = bintrayUser
    key = bintrayApiKey
    setPublications(publicationName)

    pkg(closureOf<BintrayExtension.PackageConfig> {
        repo = "maven"
        name = "vertx-koin-verticlefactory"
        userOrg = "michel-werren"
        vcsUrl = "https://gitlab.com/michel.werren/vertx-koin-verticlefactory"
        version(closureOf<BintrayExtension.VersionConfig> {
            name = project.version.toString()
            released = Date().toString()
        })
        setLicenses("MIT")
    })
}

val sourcesJar by tasks.registering(Jar::class) {
    archiveClassifier.set("sources")
    from(sourceSets.main.get().allSource)
}

publishing {
    publications {
        register(publicationName, MavenPublication::class) {
            from(components["java"])
            artifact(sourcesJar.get())
            pom {
                groupId = "ch.sourcemotion.vertx"
                artifactId = "vertx-koin-verticlefactory"
                version = project.version.toString()
                licenses {
                    license {
                        name.set("The MIT License")
                        url.set("http://www.opensource.org/licenses/MIT")
                        distribution.set("https://gitlab.com/michel.werren/vertx-koin-verticlefactory")
                    }
                }
            }
        }
    }
}

tasks.findByName("afterReleaseBuild")?.dependsOn(tasks.findByName("bintrayUpload"))