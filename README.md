# vertx-koin-verticlefactory

Vert.x verticle factory featuring Koin. Kotlin DI [Koin Github](https://github.com/InsertKoinIO/koin)

## Links

[Vert.x](http://vertx.io/docs/)

[Koin](https://insert-koin.io/)

## Getting started

This Vert.x verticle factory enables you to use dependency injection within your verticles. But please be aware that you
dont break the design principles of Vert.x [Vert.x verticles](https://vertx.io/docs/vertx-core/kotlin/#_verticles).

### Add Maven repository and dependency

```
repository {
   jcenter() 
}
```

**Groovy** 
```
dependencies {
    implementation 'ch.sourcemotion.vertx:vertx-koin-verticlefactory:x.x.x'
}
```
**Kotlin DSL** 
```
dependencies {
    implementation ("ch.sourcemotion.vertx:vertx-koin-verticlefactory:x.x.x")
}
```

### Register the factory

Before you can deploy "Koinified" verticles you have to register the factory on each Vert.x instance you plan to use it.
The most simple approach is to use the extension function provided by this library.
```
vertx.registerKoinVerticleFactory()
```

### Deploy some verticles

Before you deploy your Koin verticles you have to declare them within Koin module(s).

```
org.koin.dsl.module {
    factory { MyKoinVerticle() }
}
```

> Please note the factory instead of single { MyKoinVerticle() }. 
> If you don't plan to deploy multiple verticle instance at once you even can use single {}, but i would suggest to use
> factory { ... } in general. Because, when you provide just a single instance of your verticle and you nevertheless
> deploy multiple instances of it "DeploymentOptions().setInstances(2)". It may results in a strange behavior as verticles
> are designed in a way, that each instance (and its handlers) operates in one and the same event loop! This may breaks at least
> the absolute thread safe behavior. [Koin factories](https://insert-koin.io/docs/2.0/documentation/reference/index.html#_defining_a_factory)

The verticle name prefix of this verticle factory is "koin". This prefix is mandatory to tell Vert.x that the Koin verticle factory is
responsible to get / create verticle instances.

```
vertx.deployVerticle("koin:com.acme.MyKoinVerticle")
```
or with the helper function of the companion object of the verticle factory itself. This way it's more type safe.
```
ch.sourcemotion.vertx.koin.KoinVerticleFactory.deployKoinVerticle
```
The prefix is defined as const as well.
```
ch.sourcemotion.vertx.koin.KoinVerticleFactory.FACTORY_PREFIX
```

Now anything should be mentioned, that you can start to deploy your dependency inject enabled verticles.

Have fun