package ch.sourcemotion.vertx.koin

import io.vertx.kotlin.coroutines.CoroutineVerticle
import org.koin.dsl.module

class TestVerticleFailingStart : CoroutineVerticle() {

    companion object {
        val module = module { factory { TestVerticleFailingStart() } }
    }

    override suspend fun start() {
        throw Exception("Test failure")
    }
}