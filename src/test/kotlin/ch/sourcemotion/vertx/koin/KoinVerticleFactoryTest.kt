package ch.sourcemotion.vertx.koin

import io.kotlintest.fail
import io.vertx.core.DeploymentOptions
import io.vertx.core.Vertx
import io.vertx.junit5.VertxExtension
import io.vertx.junit5.VertxTestContext
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin

@ExtendWith(VertxExtension::class)
internal class KoinVerticleFactoryTest {

    companion object {
        const val TEST_ADDRESS = "i-am-deployed"
    }

    @BeforeEach
    internal fun setUp(vertx: Vertx) {
        vertx.registerKoinVerticleFactory()
        startKoin { modules(listOf(TestVerticle.module, TestVerticleFailingStart.module)) }
    }

    @AfterEach
    internal fun tearDown() {
        stopKoin()
    }

    @Test
    internal fun deploy_via_helper(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(2)
        vertx.eventBus().consumer<Unit>(TEST_ADDRESS) {
            checkpoint.flag()
        }
        GlobalScope.launch {
            KoinVerticleFactory.deployKoinVerticle(
                vertx,
                TestVerticle::class,
                DeploymentOptions().setInstances(2)
            )
        }.invokeOnCompletion {
            it?.let { throw it } ?: checkpoint.flag()
        }
    }

    @Test
    internal fun deploy_via_traditional(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(3)
        vertx.eventBus().consumer<Unit>(TEST_ADDRESS) {
            checkpoint.flag()
        }
        vertx.deployVerticle(
            "${KoinVerticleFactory.FACTORY_PREFIX}:${TestVerticle::class.java.name}",
            DeploymentOptions().setInstances(2), context.succeeding {
                checkpoint.flag()
            })
    }

    @Test
    internal fun deploy_not_existing_via_traditional(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        vertx.deployVerticle("${KoinVerticleFactory.FACTORY_PREFIX}:not.existing.class", context.failing {
            checkpoint.flag()
        })
    }

    @Test
    internal fun deploy_failing_start_via_traditional(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint()
        vertx.deployVerticle(
            "${KoinVerticleFactory.FACTORY_PREFIX}:${TestVerticleFailingStart::class.java.name}",
            context.failing {
                checkpoint.flag()
            })
    }

    @Test
    internal fun deploy_failing_via_helper(vertx: Vertx, context: VertxTestContext) {
        val checkpoint = context.checkpoint(1)
        GlobalScope.launch {
            KoinVerticleFactory.deployKoinVerticle(
                vertx,
                TestVerticleFailingStart::class
            )
        }.invokeOnCompletion {
            it?.let { checkpoint.flag() } ?: fail("Failing verticle should fail")
        }
    }
}