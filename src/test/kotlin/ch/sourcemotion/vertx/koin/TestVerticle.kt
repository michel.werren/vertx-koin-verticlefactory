package ch.sourcemotion.vertx.koin

import io.vertx.core.AbstractVerticle
import org.koin.dsl.module

class TestVerticle : AbstractVerticle() {

    companion object {
        val module = module { factory { TestVerticle() } }
    }

    override fun start() {
        vertx.eventBus().send(KoinVerticleFactoryTest.TEST_ADDRESS, null)
    }
}