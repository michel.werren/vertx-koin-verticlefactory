package ch.sourcemotion.vertx.koin

import io.vertx.core.Vertx

/**
 * Registers the [KoinVerticleFactory] on this Vert.x instance.
 */
fun Vertx.registerKoinVerticleFactory() {
    registerVerticleFactory(KoinVerticleFactory())
}