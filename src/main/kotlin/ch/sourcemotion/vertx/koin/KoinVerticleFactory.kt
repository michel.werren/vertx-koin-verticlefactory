package ch.sourcemotion.vertx.koin

import io.vertx.core.DeploymentOptions
import io.vertx.core.Verticle
import io.vertx.core.Vertx
import io.vertx.core.spi.VerticleFactory
import io.vertx.kotlin.core.deployVerticleAwait
import org.koin.core.KoinComponent
import org.koin.core.qualifier.Qualifier
import kotlin.jvm.internal.Reflection
import kotlin.reflect.KClass

/**
 * Vert.x [VerticleFactory] to deploy [Verticle]s managed by [org.koin.core.Koin] DI.
 */
class KoinVerticleFactory(private val qualifier: Qualifier? = null) : VerticleFactory, KoinComponent {

    companion object {
        const val FACTORY_PREFIX = "koin"

        /**
         * Helper function to deploy verticles within a coroutine.
         */
        suspend fun deployKoinVerticle(
            vertx: Vertx,
            verticleClass: KClass<out Verticle>,
            deploymentOptions: DeploymentOptions = DeploymentOptions()
        ): String = vertx.deployVerticleAwait("$FACTORY_PREFIX:${verticleClass.java.name}", deploymentOptions)
    }

    override fun createVerticle(verticleName: String, classLoader: ClassLoader?): Verticle {
        val className = VerticleFactory.removePrefix(verticleName)
        val verticleClass = classLoader?.loadClass(className) ?: Class.forName(className)
        val kotlinClass = Reflection.createKotlinClass(verticleClass)
        return getKoin().get(kotlinClass, qualifier, null)
    }

    override fun prefix() = FACTORY_PREFIX
}